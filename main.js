let currentPlayer = 'red'

let diskCount = 0

let board = [
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0]
]

const table = document.getElementById('table')

for (let i = 0; i < 7; i++) {
    let column = document.createElement('div')
    column.dataset.colIndex = i
    column.className = 'col'

    table.appendChild(column)
}

function placeDisk(event) {
    const col = event.currentTarget
    const colIndex = col.dataset.colIndex
    const rowIndex = col.childElementCount

    //placing players disks
    if (currentPlayer === 'red') {
        redDisk = document.createElement('div')
        redDisk.className = 'redDisk'
        col.appendChild(redDisk)
        board[colIndex][rowIndex] = 1
        currentPlayer = 'black'
    } else if (currentPlayer === 'black') {
        blackDisk = document.createElement('div')
        blackDisk.className = 'blackDisk'
        col.appendChild(blackDisk)
        board[colIndex][rowIndex] = 2
        currentPlayer = 'red'
    }

    //ending columns at 6 disks
    if (col.childElementCount === 6) {
        event.currentTarget.removeEventListener('click', placeDisk)
    }

    diskCount++
    console.log(board)
    checkWinner()
}

//adding click handler for columns
for (let columns of document.querySelectorAll('.col')) {
    columns.addEventListener('click', placeDisk)
}

function checkLine(a, b, c, d) {
    return ((a != 0) && (a == b) && (a == c) && (a == d))
}

function checkWinner() {


    //horizontal check
    for (let c = 0; c < 4; c++) {
        for (let r = 0; r < 6; r++) {
            if (checkLine(board[c][r], board[c + 1][r], board[c + 2][r], board[c + 3][r])) {
                if (board[c][r] === 1) {
                    alert('Red Wins')
                } else if (board[c][r] === 2) {
                    alert('Black Wins')
                }
            }
        }
    }

    //vertical check
    for (let c = 0; c < 7; c++) {
        for (let r = 0; r < 6; r++) {
            if (checkLine(board[c][r], board[c][r + 1], board[c][r + 2], board[c][r + 3])) {
                if (board[c][r] === 1) {
                    alert('Red Wins') 
                } else if (board[c][r] === 2) {
                    alert('Black Wins')
                }
            }
        }
    }

    //diagonal left/up
    for (c = 0; c < 3; c++) {
        for (r = 0; r < 3; r++) {
            if (checkLine(board[c][r], board[c + 1][r + 1], board[c + 2][r + 2], board[c + 3][r + 3])) {
                if (board[c][r] === 1) {
                    alert('Red Wins')
                } else if (board[c][r] === 2) {
                    alert('Black Wins')
                }
            }
        }
    }

    //diagonal left/down
    for (c = 3; c < 7; c++) {
        for (r = 0; r < 3; r++) {
            if (checkLine(board[c][r], board[c - 1][r + 1], board[c - 2][r + 2], board[c - 3][r + 3])) {
                if (board[c][r] === 1) {
                    alert('Red Wins')
                } else if (board[c][r] === 2) {
                    alert('Black Wins')
                }
            }
        }
    }

    //tie check
    if (diskCount === 42) {
        alert('Tie! Try Again')
    }
 }